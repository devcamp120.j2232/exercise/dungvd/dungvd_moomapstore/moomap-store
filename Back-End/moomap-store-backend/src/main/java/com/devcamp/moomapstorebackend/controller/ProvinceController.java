// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.Province;
import com.devcamp.moomapstorebackend.repository.ProvinceRepository;
import com.devcamp.moomapstorebackend.service.ProvinceService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProvinceController {
   @Autowired
   ProvinceService provinceService;
   @Autowired
   ProvinceRepository iProvince;

   public ProvinceController() {
   }

   @GetMapping({"/provinces"})
   public ResponseEntity<List<Province>> findAll() {
      try {
         List<Province> provinces = this.provinceService.getAll();
         return new ResponseEntity<>(provinces, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/provinces/{id}"})
   public ResponseEntity<Province> findProvinceById(@PathVariable("id") Integer id) {
      try {
         return new ResponseEntity<>((Province)this.iProvince.findById(id).get(), HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/provinces"})
   public ResponseEntity<Province> createProvince(@RequestBody Province province) {
      try {
         Province createdProvince = this.provinceService.createProvince(province);
         return new ResponseEntity<>(createdProvince, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping({"/provinces/{id}"})
   public ResponseEntity<Province> updateProvince(@RequestBody Province province, @PathVariable("id") int id) {
      try {
         Province updatedProvince = this.provinceService.updateProvince(province, id);
         return new ResponseEntity<>(updatedProvince, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"/provinces/{id}"})
   public ResponseEntity<Province> deleteProvince(@PathVariable("id") int id) {
      try {
         this.provinceService.deleteProvince(id);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
