// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.entity.User;
import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.repository.CustomerRepository;
import com.devcamp.moomapstorebackend.repository.UserRepository;
import com.devcamp.moomapstorebackend.security.UserPrincipal;
import com.devcamp.moomapstorebackend.service.CustomerService;
import com.devcamp.moomapstorebackend.service.ExcelExporterCustomer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CustomerController {
   @Autowired
   CustomerService customerService;

   @Autowired
   CustomerRepository iCustomer;

   @Autowired
   UserRepository iUser;

   public CustomerController() {
   }

   @GetMapping({"/customers/all"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<Customer>> getAllCustomers() {
      try {
         List<Customer> allCustomer = customerService.getAllCustomer();
         return new ResponseEntity<>(allCustomer, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/customers"})
   public ResponseEntity<Page<Customer>> getAllCustomerByPages(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
      try {
         Page<Customer> pageCustomerNavigation = customerService.getCustomerByPage(page, size);
         return new ResponseEntity<>(pageCustomerNavigation, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"customers/range"})
   public ResponseEntity<List<Customer>> getCustomersByPage(@RequestParam(value = "page",defaultValue = "0") int page, @RequestParam(value = "size",defaultValue = "10") int size) {
      try {
         List<Customer> customers = this.customerService.getCustomers(page, size);
         return ResponseEntity.ok(customers);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/customers/{customerId}"})
   public ResponseEntity<Customer> getCustomerById(@PathVariable("customerId") Integer customerId) {
      try {
         Customer customer = this.customerService.getCustomerById(customerId);
         return new ResponseEntity<>(customer, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/customers/phone/{phoneNumber}"})
   public ResponseEntity<Customer> getCustomerByPhoneNumber(@PathVariable("phoneNumber") String phoneNumber) {
      try {
         Customer customer = this.customerService.getCustomerByPhoneNumber(phoneNumber);
         return new ResponseEntity<>(customer, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/customers"})
   public ResponseEntity<Customer> createNewCustomer(@RequestBody Customer pCustomer) {
      try {
         Customer customer = this.customerService.createCustomer(pCustomer);
         return new ResponseEntity<>(customer, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // Hàm cập nhật thông tin khách hàng dành cho admin và manager
   @PutMapping({"/customers/{customerId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Object> updateCustomerByAdmin(@RequestBody Customer pCustomer, @PathVariable("customerId") Integer customerId) {
      try {
         Customer customer = this.customerService.updateCustomer(pCustomer, customerId);
         return new ResponseEntity<>(customer, HttpStatus.OK);
      } catch (IllegalArgumentException e) {
         return ResponseEntity.badRequest().body(e.getMessage());
      }
      catch (Exception var4) {
         String errorMessage = "Đã xảy ra lỗi khi cập nhật thông tin khách hàng.";
         System.out.println(var4);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
      }
   }

   // Hàm cập nhật thông tin khách hàng dành cho trang customer
   @PutMapping({"/customers/update"})
   public ResponseEntity<Object> updateCustomer(@RequestBody Customer pCustomer) {
      try {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
         UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
         User user = iUser.findByUsername(userPrincipal.getUsername());
         Customer customer = user.getCustomer();
         Customer customerUpdated = this.customerService.updateCustomer(pCustomer, customer.getId());
         return new ResponseEntity<>(customerUpdated, HttpStatus.OK);
      } catch (IllegalArgumentException e) {
         return ResponseEntity.badRequest().body(e.getMessage());
      }
      catch (Exception var4) {
         String errorMessage = "Đã xảy ra lỗi khi cập nhật thông tin khách hàng.";
         System.out.println(var4);
         return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
      }
   }

   @DeleteMapping({"/customers/{customerId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Customer> deleteCustomer(@PathVariable("customerId") Integer customerId) {
      try {
         this.customerService.deleteCustomer(customerId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping("/customers/rank")
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<Object>> getNumberOfCustomerRank() {
      try {
         List<Object> listRank = iCustomer.getNumberOfCustomerRank();
         return new ResponseEntity<>(listRank, HttpStatus.OK);
      } catch (Exception e) {
         System.out.println(e);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   // Hàm xuất ra danh sách tất cả khách hàng theo rank
   @GetMapping("/customers/export")
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public void exportCustomersByRank(HttpServletResponse response, @RequestParam(defaultValue = "") String rank) throws IOException{
         List<Customer> listCustomers = new ArrayList<>();
         System.out.println(rank);
         listCustomers = iCustomer.findAllCustomersByRank(rank);
         response.setContentType("application/octet-stream");
         DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
         String currentDateTime = dateFormatter.format(new Date());
         String headerKey = "Content-Disposition";
         String headerValue = "attachment; filename=customers_" + currentDateTime + ".xlsx";
         response.setHeader(headerKey, headerValue);
         ExcelExporterCustomer excelExporter = new ExcelExporterCustomer(listCustomers);
         excelExporter.export(response);
   }

   // Hàm lấy ra khách hàng sau khi lọc
   @GetMapping({"customers/filter"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<Page<Customer>> getCustomersFilterByPage(
                  @RequestParam(value = "page",defaultValue = "0") int page, 
                  @RequestParam(value = "size",defaultValue = "10") int size,
                  @RequestParam(value = "name", defaultValue = "") String name,
                  @RequestParam(value = "phone", defaultValue = "") String phone,
                  @RequestParam(value = "type", defaultValue = "ASC") String type,
                  @RequestParam(value = "rank", defaultValue = "") String rank) {
      try {
         Page<Customer> customers = this.customerService.getCustomerFilterByPage(name, type, rank, phone, page, size);
         return ResponseEntity.ok(customers);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
   
}
