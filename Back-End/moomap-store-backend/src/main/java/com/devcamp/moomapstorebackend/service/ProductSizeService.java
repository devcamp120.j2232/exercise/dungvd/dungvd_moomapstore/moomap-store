// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Product;
import com.devcamp.moomapstorebackend.models.ProductSize;
import com.devcamp.moomapstorebackend.repository.ProductRepository;
import com.devcamp.moomapstorebackend.repository.ProductSizeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductSizeService {
   @Autowired
   ProductSizeRepository iProductSize;
   @Autowired
   ProductRepository iProduct;

   public ProductSizeService() {
   }

   public List<ProductSize> getAllProductSizesOfProduct(Integer productId) {
      return this.iProductSize.findAllByProductId(productId);
   }

   public ProductSize createNewProductSize(Integer productId, ProductSize productSize) {
      Product product = (Product)this.iProduct.findById(productId).get();
      productSize.setProduct(product);
      productSize.setProductSold(0);
      ProductSize productSizeCreated = (ProductSize)this.iProductSize.save(productSize);
      return productSizeCreated;
   }

   public ProductSize updateProductSize(Integer productId, ProductSize pProductSize, Integer productSizeId) {
      Product product = (Product)this.iProduct.findById(productId).get();
      ProductSize productSize = (ProductSize)this.iProductSize.findById(productSizeId).get();
      productSize.setProduct(product);
      productSize.setPrice(pProductSize.getPrice());
      productSize.setSize(pProductSize.getSize());
      ProductSize productSizeUpdated = (ProductSize)this.iProductSize.save(productSize);
      return productSizeUpdated;
   }

   public void deleteProductSize(Integer productSizeId) {
      this.iProductSize.deleteById(productSizeId);
   }
}
