// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.repository;

import com.devcamp.moomapstorebackend.models.ProductSize;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ProductSizeRepository extends JpaRepository<ProductSize, Integer> {
   List<ProductSize> findAllByProductId(Integer productId);
   
   @Transactional
   @Modifying
   @Query(
      value = "DELETE FROM `product_sizes` WHERE product_id IS NULL",
      nativeQuery = true
   )
   void deleteSizesWithNullProductId();
}
