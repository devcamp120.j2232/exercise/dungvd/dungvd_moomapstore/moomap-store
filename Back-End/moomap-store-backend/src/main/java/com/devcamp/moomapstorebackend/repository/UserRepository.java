package com.devcamp.moomapstorebackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.moomapstorebackend.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    // Hàm để lọc dữ liệu người dùng thông qua username, deleted, rolekey
    @Query(value = "SELECT * FROM t_user u " +
                    "JOIN t_user_role ur ON u.id = ur.user_id " +
                    "JOIN t_role r ON ur.role_id = r.id " +
                    "WHERE u.username LIKE %:username% " +
                    "AND u.deleted LIKE %:isdeleted% " +
                    "AND r.role_key LIKE %:roleKey% ", nativeQuery = true)
    List<User> filterUser(@Param("username") String username,
                          @Param("isdeleted") Boolean isdeleted,
                          @Param("roleKey") String roleKey);

    // Hàm để lọc dữ liệu người dùng thông qua username, rolekey
    @Query(value = "SELECT * FROM t_user u " +
                    "JOIN t_user_role ur ON u.id = ur.user_id " +
                    "JOIN t_role r ON ur.role_id = r.id " +
                    "WHERE u.username LIKE %:username% " +
                    "AND r.role_key LIKE %:roleKey% ", nativeQuery = true)
    List<User> filterUserWithoutDeletedAccount(@Param("username") String username,
                                               @Param("roleKey") String roleKey);
}
