// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Customer;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExporterCustomer {
   private XSSFWorkbook workbook;
   private XSSFSheet sheet;
   private List<Customer> customers;


   public ExcelExporterCustomer(List<Customer> customers) {
      this.customers = customers;
      this.workbook = new XSSFWorkbook();
   }

   private void createCell(Row row, int columnCount, Object value, CellStyle style) {
      this.sheet.autoSizeColumn(columnCount);
      Cell cell = row.createCell(columnCount);
      if (value instanceof Integer) {
         cell.setCellValue((Integer)value);
      } else if (value instanceof Boolean) {
         cell.setCellValue((Boolean)value);
      } else {
         cell.setCellValue((String)value);
      }

      cell.setCellStyle(style);
   }

   private void writeHeaderLineCustomer() {
      this.sheet = this.workbook.createSheet("Customers");
      Row row = this.sheet.createRow(0);
      CellStyle style = this.workbook.createCellStyle();
      XSSFFont font = this.workbook.createFont();
      font.setBold(true);
      font.setFontHeight(16.0);
      style.setFont(font);
      this.createCell(row, 0, "Số thứ tự", style);
      this.createCell(row, 1, "Tên khách hàng", style);
      this.createCell(row, 2, "Số điện thoại", style);
      this.createCell(row, 3, "Email", style);
      this.createCell(row, 4, "Số nhà, đường", style);
      this.createCell(row, 5, "Xã, Phường", style);
      this.createCell(row, 6, "Huyện, thị xã", style);
      this.createCell(row, 7, "Tỉnh, thành phố", style);
      this.createCell(row, 8, "Tổng chi tiêu", style);
      this.createCell(row, 9, "Hạng", style);
   }
   
   private void writeDataLineCustomer() {
      int vSTT = 1;
      int rowCount = 1;
      CellStyle style = this.workbook.createCellStyle();
      XSSFFont font = this.workbook.createFont();
      font.setFontHeight(14.0);
      style.setFont(font);
      for(Customer customer: this.customers) {
         Row row = sheet.createRow(rowCount++);
         int columnCount = 0;
         this.createCell(row, columnCount++, vSTT++ , style);
         this.createCell(row, columnCount++, customer.getFirstName() + " " + customer.getLastName(), style);
         this.createCell(row, columnCount++, customer.getPhoneNumber(), style);
         this.createCell(row, columnCount++, customer.getEmail(), style);
         this.createCell(row, columnCount++, customer.getAddress(), style);
         this.createCell(row, columnCount++, customer.getWard(), style);
         this.createCell(row, columnCount++, customer.getDistrict(), style);
         this.createCell(row, columnCount++, customer.getProvince(), style);     
         this.createCell(row, columnCount++, customer.getAmountPayment().toString(), style);
         this.createCell(row, columnCount++, customer.getRank(), style);

      }
   }
   
   public void export(HttpServletResponse response) throws IOException {
      writeHeaderLineCustomer();
      writeDataLineCustomer();

      ServletOutputStream outputStream = response.getOutputStream();
      workbook.write(outputStream);
      workbook.close();

      outputStream.close();
   }
}
