// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.models.OrderDetail;
import com.devcamp.moomapstorebackend.models.ProductSize;
import com.devcamp.moomapstorebackend.repository.OrderDetailRepository;
import com.devcamp.moomapstorebackend.repository.OrderRepository;
import com.devcamp.moomapstorebackend.repository.ProductSizeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailService {
   @Autowired
   OrderDetailRepository iOrderDetail;
   @Autowired
   OrderRepository iOrder;
   @Autowired
   ProductSizeRepository iProductSize;

   public OrderDetailService() {
   }

   public List<OrderDetail> getAllOrderDetails() {
      return this.iOrderDetail.findAll();
   }

   public List<OrderDetail> getAllOrderDetailsOfOrder(Integer orderId) {
      return this.iOrderDetail.findAllByOrderId(orderId);
   }

   public OrderDetail createOrderDetail(Integer orderId, OrderDetail pOrderDetail, Integer sizeId) {
      Order order = (Order)this.iOrder.findById(orderId).get();
      ProductSize productSize = (ProductSize)this.iProductSize.findById(sizeId).get();
      pOrderDetail.setProductSize(productSize);
      pOrderDetail.setOrder(order);
      pOrderDetail.setProductName(productSize.getProductName());
      pOrderDetail.setSizeName(productSize.getSize());
      OrderDetail orderDetaiCreated = (OrderDetail)this.iOrderDetail.save(pOrderDetail);
      return orderDetaiCreated;
   }

   // Hàm cập nhật số lượng khi xác nhận theo order detail
   public void updateOrderDetailConfirm(OrderDetail pOrderDetail) {
      ProductSize productSize = (ProductSize)this.iProductSize.findById(pOrderDetail.getProductSize().getId()).get();
      productSize.setProductSold(productSize.getProductSold() + pOrderDetail.getQuantityOrder());
      iProductSize.save(productSize);
   }

   // Hàm cập nhật số lượng bán khi xác nhận sau đó lại hủy
   public void updateOrderDetailCancelConfirm(OrderDetail pOrderDetail) {
      ProductSize productSize = (ProductSize)this.iProductSize.findById(pOrderDetail.getProductSize().getId()).get();
      productSize.setProductSold(productSize.getProductSold() - pOrderDetail.getQuantityOrder());
      iProductSize.save(productSize);
   }
}
