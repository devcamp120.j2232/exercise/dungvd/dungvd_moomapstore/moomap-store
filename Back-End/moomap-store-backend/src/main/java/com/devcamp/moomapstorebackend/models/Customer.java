// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.devcamp.moomapstorebackend.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customers")
public class Customer {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column(name = "last_name")
   private @NotNull(message = "Input last name") String lastName;

   @Column(name = "first_name")
   private @NotNull(message = "Input first name") String firstName;

   @Column(name = "phone_number", unique = true)
   private @NotNull(message = "Input phone number") String phoneNumber;

   @Column(name = "address")
   private @NotNull(message = "Input address detail") String address;

   private String province;

   private String district;

   private String ward;

   private String rank;

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer", fetch = FetchType.LAZY)
   private List<Rating> ratings;

   @Transient
   private Long amountPayment;
   
   @Column(name = "number_orders")
   private Integer numOrder;

   private String email;

   @OneToMany(mappedBy = "customer", cascade = { CascadeType.ALL })
   @JsonIgnore
   private List<Order> orders;

   @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
   @JsonIgnore
   private User user;

   public Customer() {
      
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getLastName() {
      return this.lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public String getFirstName() {
      return this.firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getPhoneNumber() {
      return this.phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
   }

   public String getAddress() {
      return this.address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public String getProvince() {
      return this.province;
   }

   public void setProvince(String province) {
      this.province = province;
   }

   public String getDistrict() {
      return this.district;
   }

   public void setDistrict(String district) {
      this.district = district;
   }

   public String getWard() {
      return this.ward;
   }

   public void setWard(String ward) {
      this.ward = ward;
   }

   public String getEmail() {
      return this.email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public List<Order> getOrders() {
      return this.orders;
   }

   public void setOrders(List<Order> orders) {
      this.orders = orders;
   }

   public Long getAmountPayment() {
      Long sum = 0L;
      if (this.orders != null) {
         for (int i = 0; i < this.orders.size(); ++i) {
            // Đơn nào đã xác nhận mới tính vào tổng chi tiêu
            if(this.orders.get(i).getStatus().equals("confirmed")) {
               System.out.println(this.orders.get(i).getStatus());
               sum += this.orders.get(i).getAmmount();
            }
         }
      }
      return sum;
   }

   public Integer getNumOrder() {
      return numOrder;
   }

   public void setNumOrder(Integer numOrder) {
      this.numOrder = numOrder;
   }

   public String getRank() {
      return rank;
   }

   public void setRank(String rank) {
      this.rank = rank;
   }

   public User getUser() {
      return user;
   }

   public void setUser(User user) {
      this.user = user;
   }

}
