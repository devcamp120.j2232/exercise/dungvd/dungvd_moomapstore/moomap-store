// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(
   name = "order_detail"
)
public class OrderDetail {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;
   
   @ManyToOne( fetch = FetchType.LAZY)
   @JoinColumn(name = "order_id")
   @JsonIgnore
   private Order order;
   
   @ManyToOne( fetch = FetchType.LAZY)
   @JoinColumn(name = "product_size_id")
   @JsonIgnore
   private ProductSize productSize;
   
   @Column(name = "quantity_order")
   private Integer quantityOrder;
   
   @Column(name = "price_each")
   private Integer priceEach;
   

   private String productName;
   

   private String sizeName;

   public OrderDetail() {
   }

   public int getId() {
      return this.id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public Order getOrder() {
      return this.order;
   }

   public void setOrder(Order order) {
      this.order = order;
   }

   public ProductSize getProductSize() {
      return this.productSize;
   }

   public void setProductSize(ProductSize productSize) {
      this.productSize = productSize;
   }

   public Integer getQuantityOrder() {
      return this.quantityOrder;
   }

   public void setQuantityOrder(Integer quantityOrder) {
      this.quantityOrder = quantityOrder;
   }

   public Integer getPriceEach() {
      return this.priceEach;
   }

   public void setPriceEach(Integer priceEach) {
      this.priceEach = priceEach;
   }

   public String getProductName() {
      return productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public String getSizeName() {
      return sizeName;
   }

   public void setSizeName(String sizeName) {
      this.sizeName = sizeName;
   }

}
