// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.ProductSize;
import com.devcamp.moomapstorebackend.repository.ProductSizeRepository;
import com.devcamp.moomapstorebackend.service.ProductSizeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProductSizeController {
   @Autowired
   ProductSizeService productSizeService;

   @Autowired 
   ProductSizeRepository iProductSize;

   public ProductSizeController() {
   }

   @GetMapping("sizes/{productSizeId}")
   public ResponseEntity<ProductSize> getProductSizeById(@PathVariable("productSizeId") Integer productSizeId) {
      try {
         ProductSize size = this.iProductSize.findById(productSizeId).get();
         return new ResponseEntity<>(size, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"products/{productId}/sizes"})
   public ResponseEntity<List<ProductSize>> getAllSizeOfProduct(@PathVariable("productId") Integer productId) {
      try {
         List<ProductSize> listSize = this.productSizeService.getAllProductSizesOfProduct(productId);
         return new ResponseEntity<>(listSize, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"products/{productId}/sizes/"})
   public ResponseEntity<ProductSize> createProductSize(@PathVariable("productId") Integer productId, @RequestBody ProductSize pProductSize) {
      try {
         ProductSize productSize = this.productSizeService.createNewProductSize(productId, pProductSize);
         return new ResponseEntity<>(productSize, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"sizes/{sizeId}"})
   public ResponseEntity<ProductSize> deleteProductSize(@PathVariable("sizeId") Integer sizeId) {
      try {
         this.productSizeService.deleteProductSize(sizeId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
