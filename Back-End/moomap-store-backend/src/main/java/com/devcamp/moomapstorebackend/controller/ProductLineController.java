// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.ProductLine;
import com.devcamp.moomapstorebackend.service.ProductLineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProductLineController {
   @Autowired
   ProductLineService productLineService;

   public ProductLineController() {
   }

   @GetMapping({"/product-lines"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<List<ProductLine>> getAllProductLines() {
      try {
         List<ProductLine> allProductLines = this.productLineService.getAllProductLine();
         return new ResponseEntity<>(allProductLines, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/product-lines/{productLineId}"})
   public ResponseEntity<ProductLine> getProductLineById(@PathVariable("productLineId") Integer productLineId) {
      try {
         ProductLine productLine = this.productLineService.getProductLineById(productLineId);
         return new ResponseEntity<>(productLine, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/product-lines"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<ProductLine> createNewProductLine(@RequestBody ProductLine pProductLine) {
      try {
         ProductLine productLine = this.productLineService.createProductLine(pProductLine);
         return new ResponseEntity<>(productLine, HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping({"/product-lines/{productLineId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<ProductLine> updateProductLine(@RequestBody ProductLine pProductLine, @PathVariable("productLineId") Integer productLineId) {
      try {
         ProductLine productLine = this.productLineService.updateProductLine(pProductLine, productLineId);
         return new ResponseEntity<>(productLine, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"/product-lines/{productLineId}"})
   @PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
   public ResponseEntity<ProductLine> deleteProductLine(@PathVariable("productLineId") Integer productLineId) {
      try {
         this.productLineService.deleteProductLine(productLineId);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
