// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.service;

import com.devcamp.moomapstorebackend.models.Customer;
import com.devcamp.moomapstorebackend.models.Order;
import com.devcamp.moomapstorebackend.models.OrderCodeGenerator;
import com.devcamp.moomapstorebackend.models.OrderDetail;
import com.devcamp.moomapstorebackend.repository.CustomerRepository;
import com.devcamp.moomapstorebackend.repository.OrderRepository;
import com.devcamp.moomapstorebackend.repository.ProductSizeRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
   @Autowired
   OrderRepository iOrder;
   
   @Autowired
   CustomerRepository iCustomer;
   
   @Autowired
   CustomerService customerService;
   
   private final OrderCodeGenerator orderCodeGenerator;

   @Autowired
   public OrderService(OrderCodeGenerator orderCodeGenerator) {
      this.orderCodeGenerator = orderCodeGenerator;
   }

   @Autowired
   ProductSizeRepository iProductSize;

   @Autowired
   OrderDetailService orderDetailService;

   public List<Order> getAllOrder() {
      return this.iOrder.findAll();
   }

   public Order getOrderById(Integer orderId) {
      return (Order)this.iOrder.findById(orderId).get();
   }

   public List<Order> getOrdersOfCustomer(Integer customerId) {
      return this.iOrder.findAllByCustomerId(customerId);
   }

   public Order createOrderByCustomerId(Order pOrder, Integer customerId) {
      Customer customer = (Customer)this.iCustomer.findById(customerId).get();
      pOrder.setCustomer(customer);
      pOrder.setLastName(customer.getLastName());
      pOrder.setFirstName(customer.getFirstName());
      pOrder.setPhoneNumber(customer.getPhoneNumber());
      pOrder.setEmail(customer.getEmail());
      customer.setAddress(pOrder.getAddress());
      customer.setDistrict(pOrder.getDistrict());
      customer.setProvince(pOrder.getProvince());
      customer.setWard(pOrder.getWard());
      customer.setNumOrder(customer.getNumOrder() + 1);
      String orderCode = this.orderCodeGenerator.generateOrderCode();
      pOrder.setOrderCode(orderCode);
      pOrder.setStatus("processing");
      Date newDate = new Date();
      pOrder.setOrderDate(newDate);
      Order orderCreated = (Order)this.iOrder.save(pOrder);
      return orderCreated;
   }

   public Order createOrder(Order pOrder) {
      Customer customer = this.iCustomer.findByPhoneNumber(pOrder.getPhoneNumber());
      new Order();
      String orderCode = this.orderCodeGenerator.generateOrderCode();
      pOrder.setOrderCode(orderCode);
      pOrder.setStatus("processing");
      Date newDate = new Date();
      pOrder.setOrderDate(newDate);
      Customer customerUpdated;
      if (customer != null) {
         customerUpdated = this.customerService.updateCustomerByOrder(customer, pOrder);
         pOrder.setCustomer(customerUpdated);
      } else {
         customerUpdated = this.customerService.createCustomerByOrder(pOrder);
         pOrder.setCustomer(customerUpdated);
      }
      return pOrder;
   }

   public Order updateOrder(Order pOrder, Integer orderId, Integer customerId) {
      Order order = (Order)this.iOrder.findById(orderId).get();
      Customer customer = (Customer)this.iCustomer.findById(customerId).get();
      Customer customerUpdated = this.customerService.updateCustomerByOrder(customer, pOrder);
      order.setCustomer(customerUpdated);
      Order orderUpdated = (Order)this.iOrder.save(order);
      return orderUpdated;
   }

   public Order updateOrderStatus(Integer orderId, Order pOrder) {
      Order order = (Order)this.iOrder.findById(orderId).get();
      List<OrderDetail> listOrderDetails = order.getOrderDetails();
      // Nếu trạng thái hiện tại khác confirmed thì cập nhật lại đơn hàng khi đổi sang confirmed
      if(pOrder.getStatus().equals("confirmed") && !order.getStatus().equals("confirmed")) {
         for(int i = 0; i < listOrderDetails.size(); i++) {
            orderDetailService.updateOrderDetailConfirm(listOrderDetails.get(i));
         }
      }
      // Nếu đã là confirmed mà đổi sang từ chối hoặc processing thì trừ đi
      if(order.getStatus().equals("confirmed") && !pOrder.getStatus().equals("confirmed")) {
         for(int i = 0; i < listOrderDetails.size(); i++) {
            orderDetailService.updateOrderDetailCancelConfirm(listOrderDetails.get(i));
         }
      }
      order.setStatus(pOrder.getStatus());
      Customer customer = iCustomer.findByPhoneNumber(order.getPhoneNumber());
      Long vTotalPayment = customer.getAmountPayment();
      // Cập nhật thứ bậc cho khách hàng
      String vCustomerRank = "";
      if(vTotalPayment >= 5000000) {
         vCustomerRank = "plantinum";
      } 
      else if(vTotalPayment >= 2000000) {
         vCustomerRank = "gold";
      }
      else if(vTotalPayment >= 1000000) {
         vCustomerRank = "silver";
      }
      else {
         vCustomerRank = "vip";
      }
      customer.setRank(vCustomerRank);
      iCustomer.save(customer);
      Order orderUpdated = (Order)this.iOrder.save(order);
      return orderUpdated;
   }

   public void deledeOrder(Integer orderId) {
      Order order = (Order)this.iOrder.findById(orderId).get();
      List<OrderDetail> listOrderDetails = order.getOrderDetails();
      // Nếu đã là confirmed mà xóa đơn hàng thì trừ đi
      if(order.getStatus().equals("confirmed")) {
         for(int i = 0; i < listOrderDetails.size(); i++) {
            orderDetailService.updateOrderDetailCancelConfirm(listOrderDetails.get(i));
         }
      }
      this.iOrder.deleteById(orderId);
      Customer customer = iCustomer.findByPhoneNumber(order.getPhoneNumber());
      Long vTotalPayment = customer.getAmountPayment();
      String vCustomerRank = "";
      if(vTotalPayment >= 5000000) {
         vCustomerRank = "plantinum";
      } 
      else if(vTotalPayment >= 2000000) {
         vCustomerRank = "gold";
      }
      else if(vTotalPayment >= 1000000) {
         vCustomerRank = "silver";
      }
      else {
         vCustomerRank = "vip";
      }
      customer.setRank(vCustomerRank);
      iCustomer.save(customer);
   }

   public Page<Order> getOrderByPage(int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Order> orders = new ArrayList<>();
      this.iOrder.findAll(pageable).forEach(orders::add);
      Integer totalElement = (int)this.iOrder.count();
      Page<Order> orderPageNaviPage = new PageImpl<>(orders, pageable, (long)totalElement);
      return orderPageNaviPage;
   }

   public Page<Order> getOrderOfCustomerByPage(Integer customerId, int page, int size) {
      Pageable pageable = PageRequest.of(page, size);
      List<Order> orders = iOrder.findAllByCustomerId(customerId);
      int start = page * size;
      int end = Math.min(start + size, orders.size());
      Page<Order> orderPageNaviPage = new PageImpl<>(orders.subList(start, end), pageable, orders.size());
      return orderPageNaviPage;
   }


   // Hàm lọc khách hàng có phân trang
   public Page<Order> filterOrderByPage( Integer customerId, String name, String status, String type, String code, int page, int size) {
      List<Order> orders = iOrder.filterOrders(customerId, name, status, type, code);
      Pageable pageable = PageRequest.of(page, size);
      
      int start = page * size;
      int end = Math.min(start + size, orders.size());
      Page<Order> orderPageNaviPage = new PageImpl<>(orders.subList(start, end), pageable, orders.size());
      return orderPageNaviPage;
   }

}
