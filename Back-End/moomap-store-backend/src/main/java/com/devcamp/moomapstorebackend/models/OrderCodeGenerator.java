// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.models;
import org.apache.commons.lang3.RandomStringUtils;

import org.springframework.stereotype.Component;

@Component
public class OrderCodeGenerator {
    public OrderCodeGenerator() {

    }

    public String generateOrderCode() {
        int codeLength = 10;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(codeLength, useLetters, useNumbers);
    }
}
