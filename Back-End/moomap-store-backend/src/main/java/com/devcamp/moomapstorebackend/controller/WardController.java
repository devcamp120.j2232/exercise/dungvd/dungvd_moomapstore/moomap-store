// Source code is decompiled from a .class file using FernFlower decompiler.
package com.devcamp.moomapstorebackend.controller;

import com.devcamp.moomapstorebackend.models.Ward;
import com.devcamp.moomapstorebackend.repository.WardRepository;
import com.devcamp.moomapstorebackend.service.WardService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class WardController {
   @Autowired
   WardService wardService;
   @Autowired
   WardRepository iWard;

   public WardController() {
   }

   @GetMapping({"/wards"})
   public ResponseEntity<List<Ward>> findAll() {
      try {
         List<Ward> wards = this.wardService.getAll();
         return new ResponseEntity<>(wards, HttpStatus.OK);
      } catch (Exception var2) {
         System.out.println(var2);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"/wards/range"})
   public ResponseEntity<Page<Ward>> getWardByPage(@RequestParam(value = "page",defaultValue = "0") int page, @RequestParam(value = "size",defaultValue = "10") int size) {
      try {
         return new ResponseEntity<>(this.wardService.getWardByPage(page, size), HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"districts/{id}/wards"})
   public ResponseEntity<List<Ward>> getAllWardsByDistrictId(@PathVariable("id") int id) {
      try {
         return new ResponseEntity<>(this.iWard.getAllWardByDistrictId(id), HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @GetMapping({"wards/{id}"})
   public ResponseEntity<Ward> getWardById(@PathVariable("id") int id) {
      try {
         return new ResponseEntity<>((Ward)this.iWard.findById(id).get(), HttpStatus.OK);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PostMapping({"/districts/{districtId}/wards"})
   public ResponseEntity<Ward> createWard(@RequestBody Ward ward, @PathVariable("districtId") int districtId) {
      try {
         Ward createdWard = this.wardService.createWard(ward, districtId);
         return new ResponseEntity<>(createdWard, HttpStatus.OK);
      } catch (Exception var4) {
         System.out.println(var4);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @PutMapping({"/districts/{districtId}/wards/{wardId}"})
   public ResponseEntity<Ward> updateWard(@RequestBody Ward ward, @PathVariable("districtId") int districtId, @PathVariable("wardId") int wardId) {
      try {
         Ward updatedWard = this.wardService.updateWard(ward, wardId, districtId);
         return new ResponseEntity<>(updatedWard, HttpStatus.OK);
      } catch (Exception var5) {
         System.out.println(var5);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @DeleteMapping({"/wards/{id}"})
   public ResponseEntity<Ward> deleteWard(@PathVariable("id") int id) {
      try {
         this.wardService.deleteWard(id);
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception var3) {
         System.out.println(var3);
         return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
